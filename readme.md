# Calculator project with HTML, CSS and JavaScript
План действий:
1. Придумать дизайн калькулятора;
2. Создать дизайн калькулятора (данный дизайн будет создаваться с нуля в программе CorelDRAW);
3. Создать файл html, css, js (используем программу Sublime Text 3)
4. Подключить style, script в html;
5. Создать пространство, в котором будет обрабатываться JS (div);
6. Пишем калькулятор.


![](https://gitlab.com/JuliaFox/calculator/-/raw/develop/Calculator_JS.png)